#!/bin/bash

set -e

COMPOSE_DIR="clarin"
if [ $(readlink $0) ]; then
    COMPOSE_DIR=$(dirname $(readlink $0))/$COMPOSE_DIR
else
    COMPOSE_DIR=$(dirname $BASH_SOURCE)/$COMPOSE_DIR
fi

echo "Backing up Nextcloud database and data files"
cd $COMPOSE_DIR
FPM_CONTAINER=$(docker-compose ps nextcloud-fpm | grep nextcloud| cut -d ' ' -f 1)
cd ../..
if [ ! -d backups ]; then
    mkdir backups
fi
docker cp $FPM_CONTAINER:/var/www/html/data backups/
(cd backups && tar zcvf "backup_nc_$(date +%d%m%Y%H%M).tar.gz" data
if [ $? ]; then
    rm -rf data
fi)
exit 0