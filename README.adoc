= Compose configuration for the CLARIN Language Resource Switchboard
:caution-caption: ☡ CAUTION
:important-caption: ❗ IMPORTANT
:note-caption: 🛈 NOTE
:sectanchors:
:sectlinks:
:sectnumlevels: 6
:sectnums:
:source-highlighter: pygments
:tip-caption: 💡 TIP
:toc-placement: preamble
:toc:
:warning-caption: ⚠ WARNING

Docker compose deployment configuration for the CLARIN Language Resource Switchboard.

== Deploy

Checkout or download this repository.

== Run

From the project root run:
```
./control.sh start
```

By default the application's UI becomes available at:

http://localhost:8089/clrs-dev/

Other options:
```
$ ./control.sh --help

control.sh [start|stop|restart|backup|restore] [-hd]

  start       Start Language Resource Switchboard
  stop        Stop Language Resource Switchboard
  restart     Restart Language Resource Switchboard
  backup      Backup Nextcloud data directory and database
  restore     Restore Nextcloud data directory and database

  -V, --del-volumes [start|stop|restart]      Remove all volumes.
  -d, --debug Run this script in verbose mode

  -h, --help  Show help
```

== Configuration variables
Configuration options can be overriden by supplying an `.env` file in the parent directory of this project:
[source,sh]
----
COMPOSE_PROJECT_NAME=nextcloud
_SQLITE_DATABASE=nextcloud
_NEXTCLOUD_ADMIN_USER=lrsuser
_NEXTCLOUD_ADMIN_PASSWORD=lrsTestPAss
_NEXTCLOUD_ADMIN_EMAIL=example@lrs.eu
_NEXTCLOUD_TRUSTED_DOMAINS=localhost nextcloud-fpm localhost:44355
_SMTPHOST=mail
----

== Components

The docker compose file deploys a number of compoments, described in this section, as a functional service.

=== nginx server

Generic image. https://gitlab.com/CLARIN-ERIC/docker-alpine-nginx[Link to image project].

The nginx server handles all http client requests hitting the CLARIN Language Resource Switchboard.

The `nginx-config/default.conf` is supplied to this container at `/nginx_conf.d/default.conf`, together with the respective web assets comming from the `/srv/html` volume from the `Application data` container.

