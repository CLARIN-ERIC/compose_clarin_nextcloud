#!/bin/bash

set -e

COMPOSE_DIR="clarin"
if [ $(readlink $0) ]; then
    COMPOSE_DIR=$(dirname $(readlink $0))/$COMPOSE_DIR
else
    COMPOSE_DIR=$(dirname $BASH_SOURCE)/$COMPOSE_DIR
fi

if [ ! $1 == "latest" ]; then
    echo "Only the subcommand: 'restore latest' is implemented!"
    exit 1
fi

echo "Restoring most recent backup file:"
cd $COMPOSE_DIR
FPM_CONTAINER=$(docker-compose ps nextcloud-fpm | grep nextcloud| cut -d ' ' -f 1)
(cd ../../backups
FILE=$(ls -t *.tar.gz | head -1)
tar xvf "$FILE"
docker cp data $FPM_CONTAINER:/var/www/html/data
if [ $? ]; then
    rm -rf data
fi)
docker-compose exec --user root nextcloud-fpm bash -c "chown www-data:www-data /var/www/html/data"
docker-compose exec --user www-data nextcloud-fpm bash -c "php occ files:scan --all"
exit 0