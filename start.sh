#!/bin/bash

set -e

NGINX_HOST=nginx
COMPOSE_DIR="clarin"
if [ $(readlink $0) ]; then
    COMPOSE_DIR=$(dirname $(readlink $0))/$COMPOSE_DIR
else
    COMPOSE_DIR=$(dirname $BASH_SOURCE)/$COMPOSE_DIR
fi

container_is_healthy ( ) {
    # $1 : Docker compose file location
    # $2 : Service name
    if ! (cd "$1" && docker-compose ps "$2" |grep -q "(healthy)"); then
        return 1
    else
        return 0
    fi
}

if [ $(docker network ls |grep postfix_mail|wc -l)  -eq 0 ]; then
    docker network create postfix_mail
fi
cd $COMPOSE_DIR && docker-compose -f docker-compose.yml -f network-override.yml up -d
while ! container_is_healthy . ${NGINX_HOST}
do
    echo "Waiting for ${NGINX_HOST} container to become available"
    sleep 5
done

docker-compose exec --user root nextcloud-fpm sh -c "apk add bash"
#Configure SSH access
docker-compose exec --user root nextcloud-fpm sh -c "passwd -u root"
docker-compose exec --user root nextcloud-fpm sh -c "apk add openssh openrc sudo"
docker-compose exec --user root nextcloud-fpm sh -c "rc-update add sshd"
docker-compose exec --user root nextcloud-fpm sh -c "rc-status"
docker-compose exec --user root nextcloud-fpm sh -c "touch /run/openrc/softlevel"
docker-compose exec --user root nextcloud-fpm sh -c "echo 'PasswordAuthentication no' >>/etc/ssh/sshd_config"
docker-compose exec --user root nextcloud-fpm sh -c "echo 'UseDNS no' >>/etc/ssh/sshd_config"
docker-compose exec --user root nextcloud-fpm sh -c "echo 'Welcome to CLARIN Nextcloud on Alpine Linux!' > /etc/motd"
docker-compose exec --user root nextcloud-fpm sh -c "/etc/init.d/sshd start"
docker-compose exec --user root nextcloud-fpm sh -c "chown -R root:root /root/.ssh"

# configure Nexcloud instance
if ! docker-compose exec --user www-data nextcloud-fpm sh -c "php occ user:setting admin |grep -q 'settings:'"; then
    ## configure admin user
    docker-compose exec --user www-data nextcloud-fpm bash -c "export OC_PASS=\$NEXTCLOUD_ADMIN_PASSWORD;php occ user:resetpassword admin --password-from-env"
    docker-compose exec --user www-data nextcloud-fpm bash -c "php occ user:setting admin firstrunwizard show 0"
    docker-compose exec --user www-data nextcloud-fpm bash -c "php occ user:setting admin core locale 'en'"
    docker-compose exec --user www-data nextcloud-fpm bash -c "php occ user:setting admin core lang 'nl_NL'"
    docker-compose exec --user www-data nextcloud-fpm bash -c "php occ user:setting admin core timezone 'Europe/Berlin'"
    docker-compose exec --user www-data nextcloud-fpm bash -c "php occ user:setting admin settings email \$NEXTCLOUD_ADMIN_EMAIL"
    ## configure internal mail
    docker-compose exec --user www-data nextcloud-fpm bash -c "php occ config:system:set mail_from_address --value 'nextcloud'"
    docker-compose exec --user www-data nextcloud-fpm bash -c "php occ config:system:set mail_domain --value 'switchboard-clarin.eu'"
    docker-compose exec --user www-data nextcloud-fpm bash -c "php occ config:system:set mail_smtpmode --value 'smtp'"
    docker-compose exec --user www-data nextcloud-fpm bash -c "php occ config:system:set mail_smtphost --value 'mail'"
    docker-compose exec --user www-data nextcloud-fpm bash -c "php occ config:system:set mail_smtpport --value '25'"
    ## update database 
    docker-compose exec --user www-data nextcloud-fpm bash -c "php occ db:convert-filecache-bigint -n"
    ## use background cron job
    docker-compose exec --user www-data nextcloud-fpm bash -c "php occ background:cron"
    ## Disable survey and federation apps.
    docker-compose exec --user www-data nextcloud-fpm bash -c "php occ app:disable survey_client"
    docker-compose exec --user www-data nextcloud-fpm bash -c "php occ app:disable federation"
    docker-compose exec --user www-data nextcloud-fpm bash -c "php occ app:enable admin_audit"
fi
exit 0